#include <iostream>
#include <iomanip> 

#include "itensor/itensor/all.h"
#include "ITensorDMRG.h"
#include "ITensorHubbard.h"

int main(void){

std::vector<int> LL = {64};
for(auto &L :LL){
	 simul::Parameters Param;//map of pairs
	 simul::Parameter p;     //pair
	 p.second.push_back(L);
	 
	 p.first = "L";
	 p.second[0] = L;
	 Param.insert(p);
	 
	 p.first  = "n_sweeps";
	 p.second[0] = 10;
	 Param.insert(p);
	 
	 p.first  = "max_states";
	 p.second[0] = 1500;
	 Param.insert(p);
	 
	 p.first  = "t_error";
	 p.second[0] = 1e-8;
	 Param.insert(p);
	 
	 simul::Array_d t1;
	 for(int i = 0; i < L; i++){
		if(i % 2)
			t1.push_back(1);
		else
			t1.push_back(1);
	 }
	 p.second.swap(t1);
	 p.first = "t1";
	 Param.insert(p);
	 p.second.swap(t1);

	 simul::Array_d t2;
	 for(int i = 0; i < L; i++){
		if(i % 2)
			t2.push_back(0);
		else
			t2.push_back(0);
	 } 
	 p.second.swap(t2);
	 p.first = "t2";
	 Param.insert(p);
	 p.second.swap(t2);

	 simul::Array_d Us;
	 for(int i = 0; i < L; i++){
		if(i % 2)
			Us.push_back(0.0625);
		else
			Us.push_back(0.0625);
	 }
	 p.second.swap(Us);
	 p.first = "U";
	 Param.insert(p);
	 p.second.swap(Us);

	 simul::ITensorHubbard DMRG = simul::ITensorHubbard();
	 DMRG.Init(Param);
	 DMRG.build_H();
	 DMRG.set_initial_state();
	 DMRG.run();
	 
	 std::map<std::string, std::string> f_name;
	 std::pair<std::string, std::string> name;

	 name.first = "E";
	 name.second = "energy.dat";
	 f_name.insert(name);
	 
//	 name.first = "S";
//	 name.second = "entropy.dat";
//	 f_name.insert(name);
	 
	 name.first = "corr";
	 name.second = "corr.dat";
	 f_name.insert(name);
	 
	 name.first = "avr";
	 name.second = "avr.dat";
	 f_name.insert(name);
	 
	 
	 DMRG.observables(f_name);
	 DMRG.save_observables(f_name);
}	
 return 0;	
}
