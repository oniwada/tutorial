#include "ITensorDMRG.h"


namespace simul{

class ITensorHubbard : public simul::ITensorDMRG{
	public:
	ITensorHubbard():
		simul::ITensorDMRG(){}
	
	ITensorDMRG operator=(Parameters &inputs){
		return (ITensorDMRG) ITensorHubbard();
	}
	
	simul::ITensorHubbard& Init(Parameters &p){
		Param = p;
//		sites = itensor::Hubbard(Param["L"], {"ConserveNf=",false});
		sites = itensor::Hubbard(Param["L"][0], {"ConserveQNs=",true});
		
		std::pair<std::string, simul::Array_d> temp;
		temp.first = "E";
		temp.second = simul::Array_d(1, 0.0);
		Obs.insert( temp);
		
		temp.first = "S";
		temp.second = simul::Array_d(unsigned(Param["L"][0]-1), 0.0);
		Obs.insert( temp);
		
		temp.first = "corr";
		temp.second.resize(unsigned(Param["L"][0]), 0.0);
		Obs.insert( temp);
		
		temp.first = "avr";
		temp.second.resize(unsigned(Param["L"][0]), 0.0);
		Obs.insert( temp);
		
		return *this;
	}
	
	ITensorHubbard& build_H(){
		auto auto_ampo = itensor::AutoMPO(sites);
		int L = Param["L"][0];
		for(int i = 1; i < L; i++){
			double t1 = Param["t1"][i-1];
			auto_ampo += t1,"Cdagup",i+1,"Cup",i;
			auto_ampo += t1,"Cdagup",i  ,"Cup",i+1;
			
			auto_ampo += t1,"Cdagdn",i+1,"Cdn",i;
			auto_ampo += t1,"Cdagdn",i  ,"Cdn",i+1;
		}
		for(int i = 1; i < L-1; i++){
			double t2 = Param["t2"][i-1];
			auto_ampo += t2,"Cdagup",i+2,"Cup",i;
			auto_ampo += t2,"Cdagup",i  ,"Cup",i+2;
			
			auto_ampo += t2,"Cdagdn",i+2,"Cdn",i;
			auto_ampo += t2,"Cdagdn",i  ,"Cdn",i+2;
		}
		for(int i = 1; i <= L; i++){
			auto_ampo += Param["U"][i-1],"Nup",i,"Ndn",i;
		}
		
		H = itensor::toMPO(auto_ampo);
		return *this;
	}
	
	ITensorHubbard& set_initial_state(){
		auto state = itensor::InitState(sites);
		for(int i = 1; i <= Param["L"][0]; i++){
				switch(i%4){
						case 0:
							state.set(i, "Up");
							break;
						case 1:
							state.set(i, "Dn");
							break;
						case 2:
							state.set(i, "Up");
							break;
						case 3:
							state.set(i, "Dn");
							break;
				}
		}
		psi = itensor::MPS(state);
		return *this;
	}
	
	ITensorHubbard& run(){
		itensor::Sweeps sweeps = itensor::Sweeps(Param["n_sweeps"][0]);
		sweeps.maxdim() = Param["max_states"][0];
		sweeps.cutoff() = Param["t_error"][0];
	 
		auto [energy_, psi_] = itensor::dmrg(H, psi, sweeps, {"Quiet", true});
		psi = psi_;
		energy = energy_;
		
		if ( itensor::maxLinkDim(psi) > Param["max_states"][0] ){
				std::cout << "maximum bond dimension reached!!!" << std::endl;
				exit(1);
		}
		return *this;
	}
	
	ITensorHubbard& observables(std::map<std::string, std::string> save_name){
		int L2 = int(Param["L"][0]/2);
		if(save_name.find("E") != save_name.end())
			Obs["E"][0] = energy;
			
		if(save_name.find("S") != save_name.end()){
			for(int i = 1; i < Param["L"][0]; i++){
				Obs["S"][i-1] += EntanglementEntropy( i);
			}
		}
		
		if(save_name.find("corr") != save_name.end()){
			for(int i = 1; i <= Param["L"][0]; i++){
				if(i <= L2)
					Obs["corr"][i-1] += correlation( i, int(Param["L"][0]/2  ), "Ntot");
				else
					Obs["corr"][i-1] += correlation( i, int(Param["L"][0]/2+1), "Ntot");
			}
		}
		
		if(save_name.find("avr") != save_name.end()){
			for(int i = 1; i <= Param["L"][0]; i++){
				Obs["avr"][i-1] += average( i, "Ntot");
			}
		}
		return *this;
	}
};

}