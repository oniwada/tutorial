#include <iostream>
#include <vector>
#include <typeinfo>
#include <cmath>

#include "header.h"
#include "itensor/all.h"

double SzSz_Pedreiro(int i, int j, itensor::MPS &psi, itensor::SiteSet &sites){
 auto auto_ampo = itensor::AutoMPO(sites);
 auto_ampo += 1.0,"Sz",i,"Sz",j;
 itensor::MPO SziSzj = itensor::toMPO(auto_ampo);
 
 return itensor::inner(psi, SziSzj, psi);
}

double SzSz_MPS(int i, int j, itensor::MPS &psi, itensor::SiteSet &sites){
 if ( i == j){
	return 0.25;
 }
 else if( i > j){
	return SzSz_MPS(j, i, psi, sites);
 }
 
 psi.position(i);
 
 auto Sz_i = sites.op("Sz", i);
 itensor::MPS psi_d = itensor::dag(psi);
 psi_d.prime("Link");
 
 auto i_link = itensor::leftLinkIndex(psi, i); 
 auto SzSz = itensor::prime( psi(i), i_link);
 SzSz *= Sz_i;
 SzSz *= itensor::prime(psi_d(i), "Site");
 
 i++;
 for(; i < j; i++){
	SzSz *= psi(i);
	SzSz *= psi_d(i);
 }
 
 auto Sz_j = sites.op("Sz", j);
 auto j_link = itensor::rightLinkIndex(psi, j);
 SzSz *= itensor::prime(psi(j), j_link);
 SzSz *= Sz_j;
 SzSz *= itensor::prime(psi_d(j), "Site");
 
 return itensor::elt(SzSz);
}

double VnEntropy(double lambda){
 if(fabs(lambda) < 1e-12)
	return 0.0;
 else
 	return -lambda*log(lambda);
}

double EntanglementEntropy(int i, itensor::MPS &psi){
 psi.position(i);
 itensor::Index l_link = itensor::leftLinkIndex(psi, i);
 itensor::Index s = itensor::siteIndex(psi, i);

 auto [U, S, V] = itensor::svd(psi(i), {l_link, s});
 itensor::Index c_index = itensor::commonIndex(U, S);
 
 double entropy = 0.0;
 for( auto n : itensor::range1(itensor::dim(c_index))){
	double rho3 = itensor::elt(S, n, n);
	entropy += VnEntropy( rho3*rho3);
 }
 return entropy;
}

int main(void){

 int L = 100;
 std::vector<double> SzSz;
 std::vector<double> Entropy;
 itensor::SiteSet sites = itensor::SpinHalf(L);
// auto u = itensor::SpinHalf(L);

 auto auto_ampo = itensor::AutoMPO(sites);
 for(int i = 1; i < L; i++){
 	auto_ampo += 0.0,"Sz",i,"Sz",i+1;
 	auto_ampo += 0.5,"S+",i,"S-",i+1;
 	auto_ampo += 0.5,"S-",i,"S+",i+1;
 }
 itensor::MPO H = itensor::toMPO(auto_ampo);
 
 itensor::Sweeps sweeps = itensor::Sweeps(5);
 std::vector<int> ns = {-8, 4, 8, 16, 32, 16};

 for(int i = 0; i < ns.size(); i++){
	 sweeps.setmaxdim(i, ns[i]);
 }
// sweeps.maxdim(ns);
 sweeps.cutoff() = 1e-10;
 
 auto state = itensor::InitState(sites);
 for(int i = 1; i <= L; i++){
	if( (i+2)%2 ){
		state.set(i, "Up");
	}
	else{
		state.set(i, "Dn");
	}
 }
 itensor::MPS psi_0 = itensor::MPS(state);
 
 auto [energy, psi] = itensor::dmrg(H, psi_0, sweeps, {"Silent", true});
 int j = L/2;
 for(int i = 1; i <= L; i++){
	SzSz.push_back(SzSz_MPS(i, j, psi, sites));
	if( i != L)
		Entropy.push_back(EntanglementEntropy(i, psi));
 }
 
 std::ofstream e_file;
 e_file.open("entropy.dat");
 for(auto &s : Entropy)
 	e_file << s << std::endl;
 e_file.close();
 
 std::ofstream SzSz_file;
 SzSz_file.open("SzSz.dat");
 for(auto &s : SzSz)
 	SzSz_file << s << std::endl;
 SzSz_file.close();
 
 std::cout << energy << std::endl;

 return 0;
}
