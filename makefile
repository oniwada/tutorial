LIB_DIR=./itensor -I/usr/include/hdf5/serial
LINK_DIR=./itensor/lib -L/usr/lib/x86_64-linux-gnu/hdf5/serial
CFLAGS=-O2 -std=c++17 -m64 -fconcepts -fPIC 
STUFF=-litensor -lpthread -lblas -llapack -lsz -lz -ldl -lm -Wl,-rpath -Wl,/usr/lib/x86_64-linux-gnu/hdf5/serial -lhdf5 -lhdf5_hl

all: main runSpinHalfHeisenberg

main: main.cpp header.h ITensorDMRG.h
	@clear
	@clear
	g++ $(CFLAGS) -I$(LIB_DIR) -L$(LINK_DIR) -o $@ $< $(STUFF)
	
runSpinHalfHeisenberg: runSpinHalfHeisenberg.cpp ITensorDMRG.h
	@clear
	@clear
	g++ $(CFLAGS) -I$(LIB_DIR) -L$(LINK_DIR) -o $@ $< $(STUFF)
	
runHubbard: runHubbard.cpp ITensorDMRG.h ITensorHubbard.h
	@clear
	@clear
	g++ $(CFLAGS) -I$(LIB_DIR) -L$(LINK_DIR) -o $@ $< $(STUFF)

clean:
	rm main runSpinHalfHeisenberg
