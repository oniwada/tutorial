#ifndef __ITENSORDMRG__
#define __ITENSORDMRG__

#include <map>

namespace simul{

using Array_d = std::vector<double>;
using Parameters = std::map <std::string, Array_d>;
using Parameter  = std::pair<std::string, Array_d>;

class ITensorDMRG{
	public:
 Parameters Param;
 itensor::Real energy;
 itensor::MPS psi;
 itensor::SiteSet sites;
 itensor::MPO H;
 std::map<std::string, Array_d> Obs;
	
 ITensorDMRG(){}
 
 virtual ITensorDMRG operator=(Parameters &inputs){
		return ITensorDMRG();
 }
 
 virtual ITensorDMRG& Init(Parameters &inputs){
	Param = inputs;
	sites = itensor::SpinHalf(Param["L"][0]);
	
	std::pair<std::string, Array_d> temp;
	temp.first = "S";
	temp.second = Array_d(unsigned(Param["L"][0]-1), 0.0);
	Obs.insert( temp);
	
	temp.first = "SzSz";
	temp.second.resize(unsigned(Param["L"][0]), 0.0);
	Obs.insert( temp);
	return *this;
 }
 
 virtual ITensorDMRG& build_H(){
	auto auto_ampo = itensor::AutoMPO(sites);
	for(int i = 1; i < Param["L"][0]; i++){
		auto_ampo += 1.0,"Sz",i,"Sz",i+1;
		auto_ampo += 0.5,"S+",i,"S-",i+1;
		auto_ampo += 0.5,"S-",i,"S+",i+1;
	}
	H = itensor::toMPO(auto_ampo);
	return *this;
 }
 
 virtual ITensorDMRG& set_initial_state(){
	auto state = itensor::InitState(sites);
	for(int i = 1; i <= Param["L"][0]; i++){
		if( (i+2)%2 ){
			state.set(i, "Up");
		}
		else{
			state.set(i, "Dn");
		}
	}
	psi = itensor::MPS(state);
	return *this;
 }
 
 virtual ITensorDMRG& run(){
	itensor::Sweeps sweeps = itensor::Sweeps(int(Param["n_sweeps"][0]));
	
	for(int i = 0; i < Param["max_state"].size(); i++){
		sweeps.setmaxdim(i, int(Param["max_states"][i]));
	}
	for(int i = 0; i < Param["t_error"].size(); i++){
		sweeps.setcutoff(i, int(Param["t_error"][i]));
	}
 
	auto [energy_, psi_] = itensor::dmrg(H, psi, sweeps, {"Silent", true});
	energy = energy_;
	psi = psi_;
	return *this;
 }
 
 virtual ITensorDMRG& observables(std::map<std::string, std::string> save_name){
	if(save_name.find("S") != save_name.end()){
		for(int i = 1; i < Param["L"][0]; i++){
			Obs["S"][i-1] += EntanglementEntropy( i);
		}
	}
	if(save_name.find("SzSz") != save_name.end()){
		for(int i = 1; i < Param["L"][0]; i++){
			Obs["SzSz"][i-1] += correlation( i, int(Param["L"][0]/2), "Sz");
		}
	}
	return *this;
 }
 
 virtual ITensorDMRG& save_observables(std::map<std::string, std::string> save_name){
	for(auto &s: save_name){
		std::ofstream f;
		if (s.first.compare("E") == 0){
			f.open(s.second, std::ios::app);
			f << std::setprecision(15);
			f << std::scientific;
			f << Param["L"][0] << " " << Obs[s.first][0] << std::endl;
			continue;
		}
		std::cout << s.first << std::endl;
		f.open(s.second);
		f << std::setprecision(15);
		f << std::scientific;
		for(int i = 0; i < Obs[s.first].size(); i++){
			f << i+1 << " " << Obs[s.first][i] << std::endl;
		}
	}
	return *this;
 }
 
 double EntanglementEntropy(int i){
	psi.position(i);
	itensor::Index l_link = itensor::leftLinkIndex(psi, i);
	itensor::Index s = itensor::siteIndex(psi, i);

	auto [U, S, V] = itensor::svd(psi(i), {l_link, s});
	itensor::Index c_index = itensor::commonIndex(U, S);
	 
	double entropy = 0.0;
	for( auto n : itensor::range1(itensor::dim(c_index))){
		double rho3 = itensor::elt(S, n, n);
		entropy += VnEntropy( rho3*rho3);
	}
	return entropy;
 }

 double VnEntropy(double lambda){
	if(fabs(lambda) < 1e-12)
		return 0.0;
	else
		return -lambda*log(lambda);
 }
 
 double correlation(int i, int j, const std::string op){
	if ( i == j){
		psi.position(i);
		itensor::ITensor psi_d = itensor::dag(itensor::prime(psi(i), "Site"));
		itensor::ITensor psi_i = psi(i);
		itensor::ITensor Sz = sites.op(op, i);
		return itensor::elt( itensor::noPrime(psi_i*Sz)*Sz*psi_d );
	}
	else if( i > j){
		return correlation(j, i, op);
	}

	psi.position(i);
	auto Sz_i = sites.op(op, i);
	itensor::MPS psi_d = itensor::dag(psi);
	psi_d.prime("Link");
	
	auto i_link = itensor::leftLinkIndex(psi, i); 
	auto corr = itensor::prime( psi(i), i_link);
	corr *= Sz_i;
	corr *= itensor::prime(psi_d(i), "Site");
	 
	i++;
	for(; i < j; i++){
		corr *= psi(i);
		corr *= psi_d(i);
	}
	 
	auto Sz_j = sites.op(op, j);
	auto j_link = itensor::rightLinkIndex(psi, j);
	corr *= itensor::prime(psi(j), j_link);
	corr *= Sz_j;
	corr *= itensor::prime(psi_d(j), "Site");
	return itensor::elt(corr);
 }

 double average(int i, const std::string op){
	psi.position(i);
	itensor::ITensor psi_d = itensor::dag(itensor::prime(psi(i), "Site"));
	itensor::ITensor psi_i = psi(i);
	itensor::ITensor Sz = sites.op(op, i);
	return itensor::elt( psi_i*Sz*psi_d );
 }
 
 void Print_inputs(){
	for(auto &m : Param){
		std::cout << m.first << ": " << m.second[0] << std::endl;
	}
 }
};

}

#endif