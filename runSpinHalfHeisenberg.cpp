#include <iostream>
#include <iomanip> 

#include "itensor/itensor/all.h"
#include "ITensorDMRG.h"

int main(void){
	
 simul::Parameters Param;
 simul::Parameter p;
 p.second.push_back(10);
 
 p.first = "L";
 p.second[0] = 400;
 Param.insert(p);
 
 p.first  = "n_sweeps";
 p.second[0] = 10;
 Param.insert(p);
 
 p.first  = "max_states";
 p.second[0] = 400;
 Param.insert(p);
 
 p.first  = "t_error";
 p.second[0] = 1e-8;
 Param.insert(p);

 simul::ITensorDMRG DMRG = simul::ITensorDMRG();
 DMRG.Init(Param);
 DMRG.build_H();
 DMRG.set_initial_state();
 DMRG.run();
 
 std::map<std::string, std::string> f_name;
 std::pair<std::string, std::string> name;
 
 name.first = "S";
 name.second = "entropy.dat";
 f_name.insert(name);
 
 name.first = "SzSz";
 name.second = "SzSz.dat";
 f_name.insert(name);
 
 DMRG.observables(f_name);
 DMRG.save_observables(f_name);
	
 return 0;
}